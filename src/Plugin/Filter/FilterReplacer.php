<?php

/**
 * @file
 * Contains \Drupal\filter\Plugin\Filter\FilterReplacer.
 */

namespace Drupal\filter_example\Plugin\Filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Provides a filter to convert every instance of 'foo' in input text with preconfigured replacement.
 *
 * @Filter(
 *   id = "filter_autop1",
 *   title = @Translation("Every instance of 'foo' in the input text will be replaced with a preconfigured replacement."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_HTML_RESTRICTOR,
 *   settings = {
 *     "filter_example_foo" = "bar"
 *   }
 * )
 */
class FilterReplacer extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
  $settings['filter_example_foo'] = array(
    '#type' => 'textfield',
    '#title' => t('Substitution string'),
    '#default_value' => 'bar',
    '#description' => t('The string to substitute for "foo" everywhere in the text.'),
  );
  return $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $replacement = $this->settings['filter_example_foo'];
    $replacement = str_replace('foo', $replacement, $text);
    return new FilterProcessResult($replacement);
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    $replacement = isset($this->settings['filter_example_foo']) ? $this->settings['filter_example_foo'] : 'bar';
    if (!$long) {
     //This string will be shown in the content add/edit form.
    return $this->t('<em>foo</em> replaced with !replacement.', array('!replacement' => $replacement));
    }
    else {
     return $this->t('Every instance of "foo" in the input text will be replaced with a configurable value. You can configure this value and put whatever you want there. The replacement value is "!replacement".', array('!replacement' => $replacement));
    } 
  }

}
