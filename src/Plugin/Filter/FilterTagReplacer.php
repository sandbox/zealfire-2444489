<?php

/**
 * @file
 * Contains \Drupal\filter\Plugin\Filter\FilterTagReplacer.
 */

namespace Drupal\filter_example\Plugin\Filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Datetime\Date;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;


/**
 * Provides a filter to conver line breaks to HTML.
 *
 * @Filter(
 *   id = "filter_autop2",
 *   title = @Translation("Time Tag (example)"),
 *   description = @Translation("Every instance of the special &lt;time /&gt; tag will be replaced with the current date and time in the user's specified time zone."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE
 * )
 */
class FilterTagReplacer extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function prepare($text, $langcode) {
    //$text = htmlspecialchars($text);
    return preg_replace('!<time ?/>!', '[filter-example-time]', $text);
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $replacement = str_replace('[filter-example-time]', '<em>' . format_date(time()) . '</em>', $text);
    return new FilterProcessResult($replacement);
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    $this->t('<em>&lt;time /&gt;</em> is replaced with the current time.'); 
  }

}
