<?php

/**
 * @file
 * Definition of \Drupal\filter\Tests\FilterExampleTest.
 */

namespace Drupal\filter_example\Tests;

use Drupal\Component\Utility\Random;
use Drupal\simpletest\TestBase;
use Drupal\simpletest\WebTestBase;

/**
 * Functional tests for the Filter Example module.
 *
 * @group filter_example
 */
class FilterExampleTest extends WebTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = array('node','filter_example');
  protected $webUser;
  protected $filteredHtml;
  protected $fullHtml;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    //$this->filteredHtml = db_query_range('SELECT * FROM {filter_format} WHERE name = :name', 0, 1, array(':name' => 'Filtered HTML'))->fetchObject();
    //$this->fullHtml = db_query_range('SELECT * FROM {filter_format} WHERE name = :name', 0, 1, array(':name' => 'Full HTML'))->fetchObject();

    
  }

  /**
   * Functional test of the foo filter.
   * 
   * Login user, create an example node, and test blog functionality through
   * the admin and user interfaces.
   */
  public function testFilterExampleBasic() {
    

    // Enable both filters in format id 1 (default format).
    $filtered_html_format = entity_create('filter_format', array(
      'format' => 'restricted_html',
      'name' => 'Filtered HTML',
      'filters' => array(
        'filter_autop1' => array(
          'status' => 1,
        ),
        'filter_autop2' => array(
          'status' => 1,
        ),
      ),
    ));
    $filtered_html_format->save();
    // Create user.
    $this->webUser = $this->drupalCreateUser(array(
      'administer filters',
      $filtered_html_format->getPermissionName(),
      'bypass node access',
    ));
    // Login the admin user.
    $this->drupalLogin($this->webUser);
    $edit = array(
      'filters[filter_autop1][status]' => TRUE,
      'filters[filter_autop2][status]' => TRUE,
    );
    $this->drupalPostForm('admin/config/content/formats/manage/restricted_html', $edit, t('Save configuration'));

    // Create a content type to test the filters (with default format).
    $content_type = $this->drupalCreateContentType();

    // Create a test node.
    $edit = array(
      "title[0][value]" => 'alokp',
      "body[0][value]" => 'What foo is it? it is <time />',
    );
    $result = $this->drupalPostForm('node/add/' . $content_type->type, $edit, t('Save'));
    $this->assertResponse(200);
    $time = format_date(time());
    $this->assertRaw('What bar is it? it is <em>' . $time . '</em>');

    // Enable foo filter in other format id 2
    $filtered_html_format = entity_create('filter_format', array(
      'format' => 'restricted_html',
      'name' => 'Filtered HTML',
      'filters' => array(
        'filter_autop1' => array(
          'status' => 1,
        ),
      ),
    ));

    $edit = array(
      'filters[filter_autop1][status]' => TRUE,
    );
    $this->drupalPostForm('admin/config/content/formats/manage/restricted_html', $edit, t('Save configuration'));

    // Change foo filter replacement with a random string in format id 2
    $replacement = 'alokp';//TestBase::randomName();
    $options = array(
      'filters[filter_autop1][settings][filter_example_foo]' => $replacement,
    );
    $this->drupalPostForm('admin/config/content/formats/manage/restricted_html', $options, t('Save configuration'));

    // Create a test node with content format 2
    $edit = array(
      "title" => 'alokp',//TestBase::randomName(),
      "body[0][value]" => 'What foo is it? it is <time />',
    );
    $result = $this->drupalPostForm('node/add/' . $content_type->type, $edit, t('Save'));
    $this->assertResponse(200);

    // Only foo filter is enabled.
    $this->assertRaw("What " . $replacement . " is it", 'Foo filter successfully verified.');
  }
}
