<?php

/**
 * @file
 * Contains \Drupal\filter_example\Controller\filterExampleController.
 */

namespace Drupal\filter_example\Controller;

use Drupal\Core\Url;

/**
 * Class filterExampleController
 * @package Drupal\filter_example\Controller
 */

class filterExampleController {
  /**
   * A simple controller method to explain what the Filter example is about.
   */
  public function description() {
    $url = Url::fromRoute('filter.admin_overview');
    $content_add = \Drupal::l(t('admin/config/content/formats'), $url);
    $build = array(
        '#markup' => t("<p>This example provides two filters.</p><p>Foo Filter replaces
        'foo' with a configurable replacement.</p><p>Time Tag replaces the string
        '&lt;time /&gt;' with the current time.</p><p>To use these filters, go to !link and
        configure an input format, or create a new one.</p>",
          array('!link' => $content_add,)
        ),
    );
    return $build;
  }
}
